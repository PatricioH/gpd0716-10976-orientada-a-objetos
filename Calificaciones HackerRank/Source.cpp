#include <iostream>
#include <vector>

int n;

std::vector <int> grados;

int meter(int veces) {
	int meterTemp;
	for (int i = 0; i < veces; ++i) {
		std::cin >> meterTemp;
		grados.push_back(meterTemp);
	}
	return 0;
}


int redondeo() {
	int gradosTemp;
	int resta;
	for (int i = 0; i < n; ++i) {
		if (grados.at(i) < 38) {
			//no hacer nada
		}
		else {
			
			gradosTemp = grados.at(i) % 5;
			if (grados.at(i) % 5 > 2) {
				resta = 5 - gradosTemp;
				grados.at(i) += resta;
			}
		}
	}
	return 0;
}

int print() {
	for (int i = 0; i < n; ++i) {
		std::cout << grados.at(i) << "\n";
	}
	return 0;
}

int main() {

	std::cin >> n;
	grados.reserve(n);
	meter(n);
	redondeo();
	print();

	//system("pause");
	std::cin.get();

	return 0;
}