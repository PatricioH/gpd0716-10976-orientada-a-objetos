#include <iostream>

int mult(int uno, int dos) {
	if (uno == 0 || dos == 0) return 0;
	if (uno == 1) return dos;
	if (dos == 1) return uno;
	int valorAbMul = (dos < 0 ? 0 - dos : dos);
	int res = 0;
	for (int i = valorAbMul; i--;)
	{
		res += uno;
	}
	return res;
}

int main() {
	std::cout << mult(25000, 10);
	system("pause");
}