#include <iostream>
#include <vector>
using namespace std;


std::vector<int> vA = { 1,7,9,20,50 };
std::vector<int> vb = { 1,9,10 };

std::vector <int> merge(const std::vector<int> v1, const std::vector<int> v2) {
	std::vector<int> merged;
	int i = 0, j = 0;
	for (; i < v1.size() && j < v2.size();) {
		if (v1.at(i) < v2.at(j))
			merged.push_back(v1.at(i++));
		else merged.push_back(v2.at(j++));
	}
	if (v1.size() != i)
		for (; i < v1.size(); ++i)
			merged.push_back(v1.at(i));
	if (v2.size() != j)
		for (; j < v2.size(); ++j)
			merged.push_back(v2.at(j));
	return merged;
}

int main() {
	std::vector<int> merged = merge(vA, vb);
	for (int a = 0; a < merged.size(); ++a) {
		std::cout << merged[a] << "\n";
	}
	system("PAUSE");

	return 0;
}