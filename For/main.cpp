//Tarea 1 PatricioH GPD 0716 10976.
#include <iostream>
#include <conio.h>

void cicloPara() {
	for (int x = 0; x < 10; ++x) {
		std::cout << "valor de x igual a " << x << std::endl;
	}
}

int main() {

	cicloPara();
	getch();
	return 0; 

}

//Conclisiones 

/* El programa imprime desde 0, se use el incremento en "pre" o en "pos"
y el �ltimo valor impreso es el 9, adicionalmente la documentaci�n consultada
para �ste ejercicio fue la siguiente: 
https://www.tutorialspoint.com/cplusplus/cpp_for_loop.htm
donde el diagrama de flujo que explica el funcionamiento del for fue clave.*/