#include "BinarySearch.h"

int BinarySearch(const std::vector<int>& v, const int& value)
{
	int lI = 0;
	int lS = v.size() - 1;
	bool fueEncontrado = false;

	while (lI <= lS)
	{
		int middle = (lI + lS) / 2;

		if (value == v.at(middle))
		{
			fueEncontrado = true;
			return middle;
		}
		if (value > v.at(middle)) lI = middle + 1;
		else { lS = middle - 1; }
	}
	if (!fueEncontrado) return -1;
}
