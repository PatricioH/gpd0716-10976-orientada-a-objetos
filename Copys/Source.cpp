#pragma once
#include <iostream>

class Nodo
{
	int dato;
public:
	Nodo* next;

	Nodo(int valor) : dato(valor), next(nullptr)
	{
	}

	// DCopy
	Nodo(Nodo* const& valor)
	{
		this->dato = valor->dato;
		this->next = valor->next;
	}

	// SCopy
	static Nodo* ShallowCopy(Nodo* lhs, Nodo* const& rhs)
	{
		return lhs = rhs;
	}

	int getElement() const
	{
		return dato;
	}
};

class Lista
{
	Nodo* pHead;
	Nodo* pTail;
public:
	Lista()
	{
		pHead = pTail = nullptr;
	}

	void Insert(int valor)
	{
		auto temp = new Nodo(valor);
		if (pHead == nullptr)
		{
			pHead = pTail = temp;
		}
		else
		{
			pTail->next = temp;
			pTail = temp;
		}
	}

	void Delete()
	{
		if (pHead == nullptr)
		{
			std::cout << "No puedes eliminar el elemento. La lista esta vacia\n";
		}
		else
		{
			auto tempHead = pHead;
			while (tempHead != nullptr)
			{
				if (pHead == pTail)
				{
					pHead = pTail = nullptr;
					std::cout << "Elemento elimindado.\n";
				}
				else if (tempHead->next == pTail)
				{
					auto tempTail = pTail;
					pTail = tempHead;
					pTail->next = nullptr;
					delete tempTail;
					std::cout << "Elemento elimindado.\n";
				}
				tempHead = tempHead->next;
			}
		}
	}

	void Print() const
	{
		if (pHead == nullptr)
		{
			std::cout << "No puedes imprimir nada. La lista esta vacia\n";
		}
		else
		{
			auto temp = pHead;
			auto i = 1;
			std::cout << "La lista tiene los siguientes datos\n";
			while (temp != nullptr)
			{
				std::cout << "El elemento " << i << " de la lista es: " << temp->getElement() << "\n";
				temp = temp->next;
				++i;
			}
		}
	}
};

Lista lista;

int main()
{
	lista.Insert(10);
	lista.Insert(20);
	lista.Insert(30);
	lista.Insert(35);
	lista.Insert(40);
	lista.Insert(100);
	lista.Print();
	std::cout << "Press ENTER to exit.\n";
	std::cin.get();
	return 0;
}
