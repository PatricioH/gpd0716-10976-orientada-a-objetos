#pragma once
#include <iostream>
#include "Node.h"


class LinkedList {
	//friend int Node::num;
public:
	LinkedList();
	~LinkedList();
	//int head = NULL;
	Node* head;
	int size = 0;
	void insert(int num);
	void print();
	void reverse();
	
};